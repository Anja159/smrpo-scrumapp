"""
URL configuration for scrumapp_core project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.shortcuts import redirect
from django.urls import include, path
from django.conf.urls.static import static
from django.conf import settings

from scrumapp.views import ProjectDetail, ProjectList, ProjectUpdate, SprintCreate, SprintDetail, SprintUpdate, StoryCreate, StoryDetail, StoryList, StoryUpdate

urlpatterns = [
    path('projects', ProjectList.as_view(), name='project_list'),
    path('projects/<int:project_id>', ProjectDetail.as_view(), name='project_detail'),
    path('projects/<int:project_id>/edit', ProjectUpdate.as_view(), name='project_edit'),

    path('projects/<int:project_id>/sprints/<int:pk>', SprintDetail.as_view(), name='sprint_detail'),
    path('projects/<int:project_id>/sprints/create', SprintCreate.as_view(), name='sprint_create'),
    path('projects/<int:project_id>/sprints/<int:pk>/edit', SprintUpdate.as_view(), name='sprint_edit'),

    path('projects/<int:project_id>/stories', StoryList.as_view(), name='story_list'),
    path('projects/<int:project_id>/stories/<int:pk>', StoryDetail.as_view(), name='story_detail'),
    path('projects/<int:project_id>/stories/create', StoryCreate.as_view(), name='story_create'),
    path('projects/<int:project_id>/stories/<int:pk>/edit', StoryUpdate.as_view(), name='story_edit'),

    path("accounts/", include("django.contrib.auth.urls")),
    path('admin/', admin.site.urls),
    path('', lambda r: redirect('project_list'))
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

# Duplicate URL names are allowed by Django, but we don't want them in this project, so make sure they don't happen
names = [pattern.name for pattern in urlpatterns if getattr(pattern, "name", None)]
assert len(names) == len(set(names)), f"Duplicate URL names: {names}"
