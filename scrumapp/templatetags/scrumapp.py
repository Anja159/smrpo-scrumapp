from django import template
from django.urls import resolve

register = template.Library()


@register.simple_tag(name='active_if')
def active_if(request, *url_names, admin=None):
    info = resolve(request.path_info)
    current_url = info.url_name
    if admin and info.namespace != admin:
        return ''
    return ' active' if current_url in url_names else ''