from functools import cached_property

from django.contrib.auth.models import User
from django.db import models
from django.forms import ValidationError
from django.urls import reverse_lazy
from django.utils import timezone

from scrumapp.validators import date_not_in_past


class RoleEnum(models.TextChoices):
    DEVELOPER = "DEV", "Developer"
    SCRUM_MASTER = "SM", "Scrum Master"
    PRODUCT_OWNER = "PO", "Product Owner"

class ProjectMember(models.Model):
    user = models.ForeignKey("auth.User", on_delete=models.CASCADE)
    project = models.ForeignKey("Project", on_delete=models.CASCADE)
    role = models.CharField(max_length=20, choices=RoleEnum.choices)

class Project(models.Model):
    name = models.CharField(max_length=100, unique=True)
    description = models.TextField(blank=True)

    def __str__(self):
        return self.name
    
    # These use .all() to take advantage of query caching, but we still need to confirm queries are actually cached
    
    @cached_property
    def product_owner(self) -> User:
        for pm in self.projectmember_set.all():
            if pm.role == RoleEnum.PRODUCT_OWNER:
                return pm.user
            
    @cached_property
    def scrum_master(self) -> User:
        for pm in self.projectmember_set.all():
            if pm.role == RoleEnum.SCRUM_MASTER:
                return pm.user
    
    @cached_property
    def developers(self) -> list[User]:
        return [pm.user for pm in self.projectmember_set.all() if pm.role == RoleEnum.DEVELOPER]
    
    def get_absolute_url(self):
        return reverse_lazy("project_detail", kwargs={"project_id": self.id})
    

# A custom manager for Sprint so we can DRY our queries
class SprintQuerySet(models.QuerySet):
    def active(self):
        now = timezone.now()
        return self.filter(start_date__lte=now, end_date__gte=now)


class Sprint(models.Model):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    description = models.TextField(blank=True)
    start_date = models.DateField()
    end_date = models.DateField()
    active = models.BooleanField(default=False)
    velocity = models.PositiveSmallIntegerField(null=True, blank=True, verbose_name="Velocity (pts)")

    objects = SprintQuerySet.as_manager()

    def __str__(self):
        return self.name

    def clean(self):
        if self.start_date > self.end_date:
            raise ValidationError({
                "start_date": "Start date cannot be after end date"
            })
        # Only run this validation on new objects (when creating)
        if self.id is None:
            if self.start_date < timezone.now().date():
                raise ValidationError({
                    "start_date": "Start date cannot be in the past"
                })

        # TODO: turn this into a field validation
        if self.velocity == 0:
            raise ValidationError({
                "velocity": "Velocity cannot be 0"
            })

        if self.velocity > (self.end_date - self.start_date).days * 6:
            raise ValidationError({
                "velocity": "Max points in sprint duration is: " + str((self.end_date - self.start_date).days * 6)
            })
        
    def get_absolute_url(self):
        return reverse_lazy("sprint_detail", kwargs={"project_id": self.project.id, "pk": self.id})


class Story(models.Model):
    class Priority(models.IntegerChoices):
        MUST_HAVE = 1, 'Must have'
        SHOULD_HAVE = 2, 'Should have'
        COULD_HAVE = 3, 'Could have'
        WONT_HAVE = 4, "Won't have at this time"

    class Status(models.IntegerChoices):
        UNASSIGNED = 1, "Unassigned"
        ASSIGNED = 2, "Assigned"
        ACTIVE = 3, "Active"
        FINISHED = 4, "Finished"

    sprint = models.ForeignKey(Sprint, null=True, blank=True, on_delete=models.SET_NULL)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    title = models.CharField(max_length=100)
    description = models.TextField(blank=True)
    assigned_to = models.ForeignKey("auth.User", on_delete=models.SET_NULL, null=True, blank=True)
    priority = models.IntegerField(choices=Priority)
    status = models.IntegerField(choices=Status, default=1)
    value = models.FloatField()
    time_estimate = models.FloatField()

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse_lazy("story_detail", kwargs={"project_id": self.project.id, "pk": self.id})

    class Meta:
        verbose_name_plural = "stories"


class Task(models.Model):
    story = models.ForeignKey(Story, on_delete=models.CASCADE)
    title = models.CharField(max_length=100)
    description = models.TextField(blank=True)
    assigned_to = models.ForeignKey("auth.User", on_delete=models.SET_NULL, null=True, blank=True)
    hours = models.IntegerField()

    def __str__(self):
        return self.title
