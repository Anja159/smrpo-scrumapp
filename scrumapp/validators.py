from django.utils import timezone
from django.forms import ValidationError


def date_not_in_past(date):
    if date < timezone.now().date():
        raise ValidationError("Date cannot be in the past")