from django.contrib import admin

from scrumapp.models import *

###############
#   INLINES   #
###############

class ProjectMemberInline(admin.TabularInline):
    model = ProjectMember
    extra = 1
    min_num = 1

class TaskInline(admin.TabularInline):
    model = Task

####################
#   MODEL ADMINS   #
####################

@admin.register(ProjectMember)
class ProjectMemberAdmin(admin.ModelAdmin):
    list_display = ("user", "project", "role")

@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = ("name", "description")
    inlines = [ProjectMemberInline]

@admin.register(Sprint)
class SprintAdmin(admin.ModelAdmin):
    list_display = ("name", "project", "start_date", "end_date", "active")
    list_filter = ("project", "active")
    search_fields = ("name", "description")
    date_hierarchy = "start_date"

@admin.register(Story)
class StoryAdmin(admin.ModelAdmin):
    list_display = ("title", "sprint")
    list_filter = ("sprint", "project")
    search_fields = ("title", "description")
    inlines = [TaskInline]