from django.http import HttpRequest

from scrumapp.models import Project


# Add the (visible) project list to all pages
# TODO: cache this
def project_list_context(request: HttpRequest):
    ctx = {}
    if request.user.is_authenticated:
        ctx['projects'] = Project.objects.filter(projectmember__user=request.user).distinct()
    return ctx
