from typing import Any
from django import forms
from django.db.models import Q
from django.utils import timezone
from django.views.generic import FormView
from django.views.generic.edit import UpdateView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth.models import User
from scrumapp.formfields import HTML5DateInput

from scrumapp.models import Project, RoleEnum, Sprint, Story, ProjectMember


class ProjectMemberMixin(LoginRequiredMixin, PermissionRequiredMixin):
    allowed_roles = ()

    project = None

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['project'] = self.project
        return context

    def has_permission(self) -> bool:
        if not self.request.user.is_authenticated:
            return False
        
        # (DON'T PLACE ANYTHING ABOVE THIS!)
        # For order-of-execution reasons, we get the project here and save it to the view instance
        project_id = self.kwargs['project_id']
        if project_id is None:
            raise Exception("project_id not found in the URL - did you forget to include it in the URL pattern?")
        self.project = Project.objects.get(id=project_id)

        # Superuser can do anything
        if self.request.user.is_superuser:
            return True

        # Check that the user has one of the allowed roles
        if self.allowed_roles:
            for pm in self.project.projectmember_set.all():
                if pm.user_id == self.request.user.id and pm.role in self.allowed_roles:
                    return True
            return False

        return True


class ProjectList(LoginRequiredMixin, ListView):
    model = Project
    context_object_name = "projects"

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        # We're already getting the project list from the context preprocessor
        return {}


class ProjectDetail(LoginRequiredMixin, DetailView):
    model = Project
    context_object_name = "project"
    pk_url_kwarg = "project_id"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["stories"] = Story.objects.filter(project=self.object, sprint=None)
        
        context['active'] = Sprint.objects.filter(project=self.object).active()
        context['future'] = Sprint.objects.filter(project=self.object, start_date__gt=timezone.now())
        context['past'] = Sprint.objects.filter(project=self.object, end_date__lt=timezone.now())
        return context


# class ProjectMemberForm(forms.ModelForm):
#     class Meta:
#         model = ProjectMember
#         fields = ('user', 'role',)


# class ProjectMembersView(ProjectMemberMixin, FormView):
#     template_name = "scrumapp/projectmembers_form.html"
#     def get_form(self, form_class: type | None = ...) -> forms.BaseForm:
#         form = forms.modelformset_factory(ProjectMember, form=ProjectMemberForm, extra=0)
#         return form


class ProjectUpdate(ProjectMemberMixin, UpdateView):
    model = Project
    context_object_name = "project"
    template_name_suffix = "_form"
    pk_url_kwarg = "project_id"
    allowed_roles = (RoleEnum.SCRUM_MASTER)
    
    def get_form(self):
        form = super().get_form()
        form.fields['developers'].initial = User.objects.filter(projectmember__project=self.project, projectmember__role=RoleEnum.DEVELOPER)
        return form

    class form_class(forms.ModelForm):
        developers = forms.ModelMultipleChoiceField(queryset=User.objects.all(), required=False)

        class Meta:
            model = Project
            fields = ('name', 'description', 'developers')

        def save(self, commit: bool = ...) -> Any:
            # TODO: transaction
            project = super().save(commit=False)

            users = set(self.cleaned_data['developers'])

            # Very clean set stuff courtesy of Math Man™
            all = set(User.objects.filter(projectmember__project=project, projectmember__role=RoleEnum.DEVELOPER))
            to_add = users - all
            to_remove = all - users
            
            for user in to_add:
                ProjectMember.objects.create(project=project, user=user, role=RoleEnum.DEVELOPER)
            
            ProjectMember.objects.filter(project=project, user__in=to_remove).delete()
            
            return super().save(commit)

class SprintDetail(ProjectMemberMixin, DetailView):
    model = Sprint
    context_object_name = "sprint"

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['unassigned'] = Story.objects.filter(project=self.project, sprint=self.object,
            status=Story.Status.UNASSIGNED)
        ctx['assigned'] = Story.objects.filter(project=self.project, sprint=self.object,
            status=Story.Status.ASSIGNED)
        ctx['active'] = Story.objects.filter(project=self.project, sprint=self.object,
            status=Story.Status.ACTIVE)
        ctx['finished'] = Story.objects.filter(project=self.project, sprint=self.object,
            status=Story.Status.FINISHED)
        return ctx


class SprintFormView(ProjectMemberMixin, FormView):
    model = Sprint
    allowed_roles = (RoleEnum.SCRUM_MASTER)

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        return ctx

    def clean(self):
        cleaned_data = super().clean()
        start_date = cleaned_data.get("start_date")
        end_date = cleaned_data.get("end_date")
        if end_date <= start_date:
            raise forms.ValidationError("End date should be after start date.")
        has_overlap = Sprint.objects.filter(Q(project=self.project),
            Q(end_date__gte=start_date) | Q(start_date__lte=end_date)).exists()
        if has_overlap:
            raise forms.ValidationError('Start and end date overlap with another sprint')
        return cleaned_data

    def form_valid(self, form):
        form.instance.project = self.project
        return super().form_valid(form)
    
    def get_initial(self):
        return {'start_date': timezone.now(), 'end_date': timezone.now() + timezone.timedelta(days=7), 'velocity': 1}

    # We're overriding the form class to make the date fields use the HTML5 date input type. Normally we could just omit this and Django would use the default form class.
    class form_class(forms.ModelForm):
        class Meta:
            model = Sprint
            fields = ('name', 'description', 'start_date', 'end_date', 'velocity')

            widgets = {
                'start_date': HTML5DateInput(),
                'end_date': HTML5DateInput(),
            }

class SprintCreate(SprintFormView, CreateView):
    template_name_suffix = '_form'


class SprintUpdate(SprintFormView, UpdateView):
    template_name_suffix = '_form'


class StoryList(ProjectMemberMixin, ListView):
    model = Story
    context_object_name = "story"


class StoryDetail(ProjectMemberMixin, DetailView):
    model = Story
    context_object_name = "story"


class StoryFormView(ProjectMemberMixin, FormView):
    model = Story
    fields = ('sprint', 'title', 'description', 'assigned_to', 'priority', 'value', 'time_estimate')
    allowed_roles = (RoleEnum.SCRUM_MASTER, RoleEnum.PRODUCT_OWNER)

    def get_form(self):
        form = super().get_form()
        form.fields['sprint'].queryset = Sprint.objects.filter(project=self.project)
        return form

    def clean(self):
        cleaned_data = super().clean()
        same_title_exists = Story.objects.filter(project=self.project,
            title__iexact=self.object.title).exists()
        if same_title_exists:
            raise forms.ValidationError('There is another story with the same title')
        return cleaned_data

    def form_valid(self, form):
        form.instance.project = self.project
        return super().form_valid(form)


class StoryCreate(StoryFormView, CreateView):
    template_name_suffix = '_form'


class StoryUpdate(StoryFormView, UpdateView):
    template_name_suffix = '_form'
