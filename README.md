# SMRPO projekt: ScrumApp

## Development

### First time setup

1. Install Python 3.11
    - 3.11 and 3.12 will probably work too
    - to use a different version, remove the version lock from `Pipfile`
2. Install PipEnv (`pip install pipenv`)
3. Install dependencies (`pipenv install`)
    - a virtualenv will be created in your home dir
    - enter it in a terminal using `pipenv shell`
4. Set your IDE to use the virtualenv
    - in VS Code use `Python: Select Interpreter`
    - in PyCharm in project settings add a new interpreter of type "Pipenv"
5. Run the database migrations (`python manage.py migrate`)
    - also do this after every checkout of new code

### When developing

**To run the development server:** `python manage.py runserver`. If using an IDE, you should probably use the built-in Django runner.

**To make the migrations after a model change:** `python manage.py makemigrations`.   
You should probably run a preview first: `python manage.py makemigrations --dry-run`

**Adding new stuff:**

  - add new entity types ("models") in `models.py`
  - if you want it in the admin page, make and register a `ModelAdmin` in `admin.py`
  - if the entity needs its own page:
    - add a `DetailView` for that model in `views.py`
    - add a template for that view in `templates/scrumapp/{model_name}_detail.html`
  - if the entity needs a listing page:
    - add a `ListView` for that model in `views.py`
    - add a template calling that partial in `templates/scrumapp/{model_name}_list.html`
  - if the entity needs to be listed in multiple places:
    - add a partial template in `templates/scrumapp/partial/{model_name}_list.html`
    - include that partial in all templates where the listing is used
  - all views that are connected to a project should inherit `ProjectMemberMixin`
  - add all the new views to `urls.py`
    - if it's a `DetailView`, add a `get_absolute_url` method to its model (use `reverse()`)
